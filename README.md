# Outside Open Infrastructure Modules for Terragrunt

This repo is used by Terragrunt to help keep the Terraform code DRY.

Currently we only support `aws`, but the directory set up is designed to allow other providers to be created later.

## How do you use these modules?

To use a module, create a `terragrunt.hcl` file that specifies the module you want to use as well as values for the
input variables of that module:

```hcl
# Use Terragrunt to download the module code
terraform {
  source = "git=https://bitbucket.org/outsideopen/terragrunt-modules.git//path/to/module?ref=v0.0.1"
}

# Fill in the variables for that module
inputs = {
  foo = "bar"
  baz = 3
}
```