variable domain {
  type = string
}

variable tags {
  type = map(string)
}