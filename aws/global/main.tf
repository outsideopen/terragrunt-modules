locals {
  default_tags = {
    ManagedBy = "Terraform"
  }
  tags = merge(local.default_tags, var.tags)
}

resource aws_ebs_encryption_by_default ebs_encryption {
  enabled = true
}

resource aws_route53_zone domain {
  name = var.domain
  tags = local.tags
}

// These are used by chamber to manage secrets
resource aws_kms_key parameter_store {
  description             = "Parameter store kms master key"
  deletion_window_in_days = 10
  enable_key_rotation     = true
  tags                    = local.tags
}

resource aws_kms_alias parameter_store_alias {
  name          = "alias/parameter_store_key"
  target_key_id = aws_kms_key.parameter_store.id
}