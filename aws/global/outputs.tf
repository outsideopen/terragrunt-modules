output zone_name {
  value = aws_route53_zone.domain.name
}

output zone_id {
  value = aws_route53_zone.domain.id
}

output chamber_key_id {
  value = aws_kms_key.parameter_store.id
}

output chamber_key_arn {
  value = aws_kms_key.parameter_store.arn
}