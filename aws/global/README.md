# AWS Global settings

Global settings for an AWS account

- Sets **EBS Encryption by default** to **true**
- Creates a global **Route 53 zone** available to all environments
- Sets up **alias/parameter_store_key** used by [Chamber](https://github.com/segmentio/chamber)
  - Access to parameters is controlled per environment

## inputs

| name   | description |
|--------|-------------|
| domain | The zone name of the Route 53 zone to create |
| tags   | List of tags to add |

## outputs

| name |  
|------|
| zone_name | Route 53 zone name |
| zone_id   | Route 53 zone id |
| chamber_key_id | KMS Key ID used by Chamber |
| chamber_key_arn | KMS Key ARN used by Chamber |