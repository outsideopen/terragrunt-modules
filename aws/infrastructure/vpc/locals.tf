## local variables
locals {
  # bits for the subnet calculations
  az_cidr = {
    1 = 1
    2 = 1
    3 = 2
    4 = 2
    5 = 3
    6 = 3
  }
  azs          = length(var.azs)
  public_cidr  = cidrsubnet(var.cidr, 1, 0)
  public_cidrs = [
    for i in range(0, local.azs) : cidrsubnet(local.public_cidr, local.az_cidr[local.azs], i)
  ]
  private_cidr  = cidrsubnet(var.cidr, 1, 1)
  private_cidrs = [
    for i in range(0, local.azs) : cidrsubnet(local.private_cidr, local.az_cidr[local.azs], i)
  ]
}