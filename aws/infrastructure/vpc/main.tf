module "label" {
  source      = "cloudposse/label/null"
  version     = "0.25.0"
  namespace   = var.namespace
  environment = var.environment
  stage       = var.stage
  name        = "vpc"
  tags        = var.tags
}

data "aws_security_group" "default" {
  name   = "default"
  vpc_id = module.vpc.vpc_id
}

locals {
  private_subnet_tags = var.enable_eks_tags ? {
    "kubernetes.io/role/internal-elb" = "1"
  } : {}
  public_subnet_tags = var.enable_eks_tags ? {
    "kubernetes.io/role/elb" = "1"
  } : {}
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.78.0"
  name    = module.label.id
  cidr    = var.cidr
  azs     = var.azs

  private_subnets      = local.private_cidrs
  private_subnet_tags  = merge(local.private_subnet_tags, var.private_subnet_tags)
  public_subnets       = local.public_cidrs
  public_subnet_tags   = merge(local.public_subnet_tags, var.public_subnet_tags)
  enable_dns_hostnames = true
  tags                 = {
    for k, v in module.label.tags : k => v if k != "Name"
  }

  enable_nat_gateway     = var.enable_nat_gateway
  single_nat_gateway     = var.single_nat_gateway
  one_nat_gateway_per_az = var.enable_nat_gateway ? var.one_nat_gateway_per_az : false
  nat_eip_tags           = module.label.tags
  nat_gateway_tags       = module.label.tags
}

module "nat" {
  count   = var.enable_nat_instance ? 1 : 0
  source  = "int128/nat-instance/aws"
  version = "2.1.0"

  enabled                     = var.enable_nat_instance
  name                        = "${module.label.id}-nat"
  vpc_id                      = module.vpc.vpc_id
  public_subnet               = module.vpc.public_subnets[0]
  private_subnets_cidr_blocks = module.vpc.private_subnets_cidr_blocks
  private_route_table_ids     = module.vpc.private_route_table_ids
}
