### common variables
variable namespace {}
variable environment {}
variable stage {
  default = null
}
variable tags {
  default = {}
}

### VPC variables
variable cidr {}
variable azs {
  type = list
}

variable enable_eks_tags {
  default = false
}
variable private_subnet_tags {
  type = map
  default = {}
}
variable public_subnet_tags {
  type = map
  default = {}
}
### NAT gateway settings
variable enable_nat_instance {
  default = false
}
variable enable_nat_gateway {
  default = true
}
variable single_nat_gateway {
  default = true
}
variable one_nat_gateway_per_az {
  default = false
}
variable nat_gateway_enabled {
  default = true
}

### endpoint settings
variable enable_logs_endpoint {
  default = false
}

variable enable_s3_endpoint {
  default = true
}