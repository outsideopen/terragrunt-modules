output id {
  value = module.vpc.vpc_id
}

output cidr {
  value = var.cidr
}

output azs {
  value = var.azs
}

output subnets {
  value = {
    public  = module.vpc.public_subnets
    private = module.vpc.private_subnets
  }
}

output subnet_cidrs {
  value = {
    public  = local.public_cidrs
    private = local.private_cidrs
  }
}