resource aws_kms_key kms {
  description = "${module.this.id} kms key"
  tags        = module.this.tags
}

resource aws_kms_alias kms {
  name          = "alias/${var.environment}"
  target_key_id = aws_kms_key.kms.key_id
}

locals {
  domain_name = "${var.environment}.${var.zone_name}"
}

module ssl_cert {
  # this is forked until PR-33 is merged (https://github.com/cloudposse/terraform-aws-acm-request-certificate/pull/33)
  source                            = "./cloudposse-acm-certificate"
  zone_name                         = var.zone_name
  domain_name                       = local.domain_name
  process_domain_validation_options = var.zone_auto_register
  ttl                               = "300"
  subject_alternative_names         = flatten([["*.${local.domain_name}"],var.ssl_extra_san])
}