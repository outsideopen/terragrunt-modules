variable account_id {
  type        = string
  description = "The AWS Account ID"
}

variable zone_name {
  type        = string
  description = "The Route53 zone name to register SSL CNAMEs with"
}

variable zone_auto_register {
  description = "Whether or not to auto register with Route53"
  default     = true
}

variable ssl_extra_san {
  type    = list(string)
  default = []
}