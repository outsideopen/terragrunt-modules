output kms_key_id {
  value = aws_kms_key.kms.key_id
}

output kms_key_alias {
  value = aws_kms_alias.kms.name
}

output certificate_arn {
  value = module.ssl_cert.arn
}

output certificate_id {
  value = module.ssl_cert.id
}