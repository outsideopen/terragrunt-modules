module "peer" {
  source  = "cloudposse/vpc-peering/aws"
  version = "0.6.0"
  namespace        = var.namespace
  stage            = var.environment
  name             = "to-${var.name}"
  requestor_vpc_id = var.from_vpc_id
  acceptor_vpc_id  = var.to_vpc_id
}