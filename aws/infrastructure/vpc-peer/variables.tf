### common variables
variable namespace {}
variable environment {}
variable name {}

### VPC variables
variable from_vpc_id {}
variable to_vpc_id {}