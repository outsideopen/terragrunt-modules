output id {
  value = aws_route53_zone.internal.id
}

output name {
  value = aws_route53_zone.internal.name
}