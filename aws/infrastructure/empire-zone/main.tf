resource aws_route53_zone internal {
  name = "${var.environment}-empire"
  vpc {
    vpc_id = var.vpc_id
  }
}