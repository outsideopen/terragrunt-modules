variable key_data {}

variable enable_ecr {
  type = bool
  default = false
}
variable ecr_name {}