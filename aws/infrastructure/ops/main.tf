resource aws_key_pair ansible {
  key_name   = "ansible"
  public_key = var.key_data
}

resource aws_ecr_repository ecr {
  count                = var.enable_ecr ? 1 : 0
  name                 = var.ecr_name
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource aws_iam_role waypoint {
  name = "waypoint-service"
  assume_role_policy = ""
}

locals {
  policy_arns = [
    // give it read-only to everything as we don't know what it might really need to review
    "arn:aws:iam::aws:policy/ReadOnlyAccess"
  ]
}
resource aws_iam_role_policy_attachment waypoint_arns {
  for_each = toset(local.policy_arns)
  policy_arn = each.value
  role       = aws_iam_role.waypoint.name
}
