locals {
	tags = {
		environment = var.env
	}
	s3_log_bucket = "${var.env}-alb"
}

module "s3" {
  source  = "cloudposse/lb-s3-bucket/aws"
  version = "0.2.0"

  acl       = "private"
  name      = var.log_bucket_name
  region    = var.region
  tags      = merge(local.tags, var.tags)
}

module "alb" {
  source                     = "terraform-aws-modules/alb/aws"

  name                       = "${var.name}-alb"
  vpc_id                     = var.vpc_id
  subnets                    = var.subnets
  security_groups            = compact(var.security_group_ids)

  access_logs = {
    bucket = var.log_bucket_name
  }

  target_groups = [
    {
      name_prefix      = "${var.name}-"
      backend_protocol = "HTTP"
      backend_port     = var.container_port
      target_type      = "instance"
    }
  ]

  https_listeners = [
    {
      port               = 443
      protocol           = "HTTPS"
      certificate_arn    = var.ssl_certificate_arn
      target_group_index = 0
    }
  ]

  enable_http2                = true
  enable_deletion_protection  = true
  tags                        = merge(local.tags, var.tags)
  listener_ssl_policy_default = var.ssl_policy
}

resource "aws_lb_listener" "front_end_http_redirect" {
  load_balancer_arn = module.alb.this_lb_id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_target_group_attachment" "http_target_group" {
  count            = length(var.instances)
  target_group_arn = module.alb.target_group_arns[0]
  target_id        = var.instances[count.index]
  port             = 80
}

resource "aws_lb_target_group_attachment" "https_target_group" {
  count            = length(var.instances)
  target_group_arn = module.alb.target_group_arns[0]
  target_id        = var.instances[count.index]
  port             = 80
}

resource "aws_route53_record" "alias" {
  count = length(var.zone_id) > 0 ? 1 : 0

  zone_id = var.zone_id
  name    = var.domain_name
  type    = "A"

  alias {
    name                    = module.alb.this_lb_dns_name
    zone_id                 = module.alb.this_lb_zone_id
    evaluate_target_health  = false
  }
}