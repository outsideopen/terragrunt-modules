output "id" {
	value = module.alb.this_lb_id
}

output "lb_zone_id" {
  value = module.alb.this_lb_zone_id
}

output "lb_dns_name" {
  value = module.alb.this_lb_dns_name
}

output listener_arns {
  value = module.alb.https_listener_arns
}