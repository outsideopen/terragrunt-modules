variable "name" {
	type = string
}

variable "region" {
  type = string
}

variable "env" {
	type = string
}

variable "subnets" {
  type = list
}

variable "tags" {
  type    = map
  default = {}
}

variable "vpc_id" {
  type = string
}

variable "ssl_certificate_arn" {
  type = string
}

variable "ssl_policy" {
  type    = string
  default = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
}

variable "instances" {
  type = list(string)
}

variable "security_group_ids" {
  type    = list
  default = null
}

variable "zone_id" {
  type    = string
  default = null
}

variable "domain_name" {
  type    = string
  default = null
}

variable "log_bucket_name" {
  type = string
}

variable container_port {

}