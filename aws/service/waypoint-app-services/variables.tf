variable account_id {
  type        = string
  description = "The AWS Account ID"
}

variable kms_key_id {
  type        = string
  description = "The KMS Key ID"
}

variable app_name {
  type        = string
  description = "Waypoint App name"
}

variable app_port {
  description = "Waypoint App port"
  default     = 3000
}

variable region {
  type        = string
  description = "Region for the ALB"
}

variable vpc_id {
  type        = string
  description = "VPC ID for the ALB"
}

variable public_subnets {
  type        = list(string)
  description = "List of public subnets"
}

variable private_subnets {
  type        = list(string)
  description = "List of private subnets"
  default     = []
}

variable ssl_policy {
  type        = string
  description = "The AWS ALB certificate policy"
  default     = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
}
variable certificate_arn {
  type        = string
  description = "Arn for the SSL certificate."
  default     = null
}

variable task_policies {
  default = []
}

variable task_policy_arns {
  type        = list(string)
  description = "List of policy ARN's to attach to the task role"
  default     = []
}

variable execution_policies {
  default = []
}

variable execution_policy_arns {
  type        = list(string)
  description = "List of policy ARN's to attach to the execution role"
  default     = []
}


variable retain_logs_in_days {
  default = 7
}

### ssl self signed settings
variable use_self_signed_ssl {
  type        = bool
  description = "Whether or not to use a self signed certificate"
  default     = false
}
variable ssl_host_names {
  type    = list(string)
  default = []
}
variable ssl_common_name {}
variable ssl_organization {}