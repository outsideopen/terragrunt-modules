resource aws_cloudwatch_log_group cluster {
  name              = module.this.id
  tags              = module.this.tags
  retention_in_days = var.retain_logs_in_days
}
