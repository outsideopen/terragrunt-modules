output address {
  value = module.alb.alb_dns_name
}

output listener_arn {
  value = module.alb.listener_arns[0]
}

output task_role_name {
  value = module.task_role.name
}

output task_role_arn {
  value = module.task_role.arn
}

output log_group_name {
  value = aws_cloudwatch_log_group.cluster.name
}

output log_group_arn {
  value = aws_cloudwatch_log_group.cluster.arn
}