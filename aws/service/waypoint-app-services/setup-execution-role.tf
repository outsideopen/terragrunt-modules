locals {
  execution_role_policy_arns = distinct(concat(var.execution_policy_arns, [
    "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role",
    "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  ]))
}

module execution_role {
  source  = "cloudposse/iam-role/aws"
  version = "0.6.1"
  context = module.this.context
  name    = "execution-role"

  role_description   = "Waypoint Task Execution Role for ${module.this.id}"
  policy_description = "Waypoint Task Execution Role"

  assume_role_action = "sts:AssumeRole"
  principals         = {
    Service = [
      "ec2.amazonaws.com",
      "ecs-tasks.amazonaws.com"
    ]
  }

  policy_document_count = length(var.execution_policies)
  policy_documents      = var.execution_policies
}


data aws_iam_policy_document secrets_access {
  statement {
    sid       = "secretsAccess"
    actions   = [
      "ssm:GetParameters",
      "secretsmanager:GetSecretValue",
      "kms:Decrypt"
    ]
    resources = [
      "arn:aws:ssm:${var.region}:${var.account_id}:parameter/${var.environment}/*",
      "arn:aws:secretsmanager:${var.region}:${var.account_id}:secret:/${var.environment}/*",
      "arn:aws:kms:${var.region}:${var.account_id}:key/${var.kms_key_id}"
    ]
  }
}

resource aws_iam_policy secrets_access {
  name        = "${module.execution_role.name}-secrets-access"
  description = "Allows secrets access to the ${module.execution_role.name}"
  policy      = data.aws_iam_policy_document.secrets_access.json
}

resource aws_iam_role_policy_attachment task_role_secret_policy {
  role       = module.execution_role.name
  policy_arn = aws_iam_policy.secrets_access.arn
}

resource aws_iam_role_policy_attachment execution_role_policies {
  for_each   = toset(local.execution_role_policy_arns)
  role       = module.execution_role.name
  policy_arn = each.value
}