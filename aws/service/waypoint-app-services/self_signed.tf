// this is to get it up and running
resource tls_private_key self_signed_ssl {
  count     = var.use_self_signed_ssl ? 1 : 0
  algorithm = "RSA"
  rsa_bits  = 2048
}

resource tls_self_signed_cert self_signed_ssl {
  count                 = var.use_self_signed_ssl ? 1 : 0
  key_algorithm         = "RSA"
  private_key_pem       = tls_private_key.self_signed_ssl.0.private_key_pem
  subject {
    common_name  = var.ssl_common_name
    organization = var.ssl_organization
  }
  // 6 months
  dns_names             = flatten([[var.ssl_common_name], var.ssl_host_names])
  validity_period_hours = 4320
  allowed_uses          = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]
}

resource aws_acm_certificate ssl_cert {
  count            = var.use_self_signed_ssl ? 1 : 0
  private_key      = tls_private_key.self_signed_ssl.0.private_key_pem
  certificate_body = tls_self_signed_cert.self_signed_ssl.0.cert_pem
}