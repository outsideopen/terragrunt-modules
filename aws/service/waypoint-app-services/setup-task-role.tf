locals {
  // currently required
  task_policy_arns = distinct(concat(var.task_policy_arns, [
    "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  ]))
}

module task_role {
  source  = "cloudposse/iam-role/aws"
  version = "0.6.1"
  context = module.this.context
  name    = "${var.name}-task-role"

  role_description   = "${module.this.id} Task Role"
  policy_description = "${module.this.id} Task Role"

  assume_role_action = "sts:AssumeRole"
  principals         = {
    Service = [
      "ecs-tasks.amazonaws.com"
    ]
  }

  policy_document_count = length(var.task_policies)
  policy_documents      = var.task_policies
}

resource aws_iam_role_policy_attachment task_role_policies {
  for_each   = toset(local.task_policy_arns)
  role       = module.task_role.name
  policy_arn = each.value
}