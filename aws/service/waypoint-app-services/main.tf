locals {
  tags            = merge(module.this.tags, {
    Name = "${module.this.id}-alb"
  })
  certificate_arn = var.use_self_signed_ssl ? aws_acm_certificate.ssl_cert.0.arn : var.certificate_arn
}

resource aws_ecs_cluster cluster {
  name = module.this.id
  tags = module.this.tags
}

module "alb" {
  source  = "cloudposse/alb/aws"
  version = "0.19.0"
  # common
  name    = module.this.id
  tags    = local.tags

  # VPC
  subnet_ids         = var.public_subnets
  vpc_id             = var.vpc_id
  security_group_ids = [
    module.app_inbound.this_security_group_id
  ]

  # logs
  access_logs_prefix = module.this.id
  access_logs_region = var.region

  # https
  http2_enabled    = true
  https_enabled    = true
  https_ssl_policy = ""
  certificate_arn  = local.certificate_arn

  # target_groups (this is not necessary as Waypoint will override)
  target_group_additional_tags = local.tags
  target_group_port            = var.app_port
}

module app_inbound {
  source          = "terraform-aws-modules/security-group/aws"
  version         = "3.16.0"
  name            = "${var.app_name}-inbound"
  use_name_prefix = false
  tags            = merge(module.this.tags, {
    Name = "${var.app_name}-inbound"
  })
  description     = "HTTP/HTTPS access for ${var.environment}"
  vpc_id          = var.vpc_id

  ingress_rules       = [
    "http-80-tcp",
    "https-443-tcp"
  ]
  ingress_cidr_blocks = [
    "0.0.0.0/0"
  ]
  egress_rules        = [
    "all-all"
  ]
  egress_cidr_blocks  = [
    "0.0.0.0/0"
  ]

  # disable ipv6
  ingress_ipv6_cidr_blocks = []
  egress_ipv6_cidr_blocks  = []
}

module app_inbound_internal {
  source          = "terraform-aws-modules/security-group/aws"
  version         = "3.16.0"
  name            = "${var.app_name}-inbound-internal"
  use_name_prefix = false
  tags            = merge(module.this.tags, {
    Name = "${var.app_name}-inbound-internal"
  })
  description     = "HTTP/HTTPS access for ${var.environment}"
  vpc_id          = var.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port   = var.app_port
      to_port     = var.app_port
      protocol    = "tcp"
      description = "${var.app_name} port"
      cidr_blocks = "0.0.0.0/0"
    }
  ]

  egress_rules       = [
    "all-all"
  ]
  egress_cidr_blocks = [
    "0.0.0.0/0"
  ]

  # disable ipv6
  ingress_ipv6_cidr_blocks = []
  egress_ipv6_cidr_blocks  = []
}