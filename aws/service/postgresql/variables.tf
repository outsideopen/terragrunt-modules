### common variables
variable namespace {}
variable environment {}
variable name {
  default = "postgres"
}
variable kms_key_id {}

### VPC variables
variable vpc_id {}
variable allowed_security_groups {
  type    = list(string)
  default = []
}
variable allowed_cidr_blocks {
  type    = list(string)
  default = []
}
variable subnets {
  type = list(string)
}

## instance settings
variable multi_az {
  default = false
}
variable storage_size {
  default = 5
}
variable instance_type {
  default = "db.t3.micro"
}
variable engine_version {
  default = "12"
}
variable parameter_group {
  default = "postgres12"
}

## base database settings
variable admin_password {}
variable admin_user {
  default = "pgadmin"
}
variable db_name {
  default = "postgres"
}