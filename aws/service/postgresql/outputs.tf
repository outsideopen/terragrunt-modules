output address {
  value = module.db.instance_address
}

output arn {
  value = module.db.instance_arn
}

output endpoint {
  value = module.db.instance_endpoint
}

output id {
  value = module.db.instance_id
}

output param_id_arn {
  value = aws_ssm_parameter.db_id.arn
}

output param_host_arn {
  value = aws_ssm_parameter.db_host.arn
}

output param_endpoint_arn {
  value = aws_ssm_parameter.db_endpoint.arn
}

output param_password_arn {
  value = aws_secretsmanager_secret_version.db_admin.arn
}

output param_password_version_id {
  value = aws_secretsmanager_secret_version.db_admin.version_id
}