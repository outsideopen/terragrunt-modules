module "db" {
  source    = "cloudposse/rds/aws"
  version   = "0.23.0"
  namespace = var.namespace
  stage     = var.environment
  name      = "postgres"

  multi_az = var.multi_az

  # vpc
  subnet_ids          = var.subnets
  vpc_id              = var.vpc_id
  allowed_cidr_blocks = var.allowed_cidr_blocks
  security_group_ids  = var.allowed_security_groups
  publicly_accessible = false

  # security
  storage_encrypted = true

  # instance
  allocated_storage  = var.storage_size
  db_parameter_group = var.parameter_group
  engine             = "postgres"
  engine_version     = var.engine_version
  instance_class     = var.instance_type

  # database
  database_password = var.admin_password
  database_user     = var.admin_user
  database_name     = var.db_name
  database_port     = 5432
}

resource aws_ssm_parameter db_id {
  name        = "/${var.environment}/postgres/id"
  description = "${var.environment} postgresql id"
  type        = "String"
  value       = module.db.instance_id
}

resource aws_ssm_parameter db_host {
  name        = "/${var.environment}/postgres/host"
  description = "${var.environment} postgresql host"
  type        = "String"
  value       = module.db.instance_address
}

resource aws_ssm_parameter db_endpoint {
  name        = "/${var.environment}/postgres/endpoint"
  description = "${var.environment} postgresql endpoint"
  type        = "String"
  value       = module.db.instance_endpoint
}

resource aws_secretsmanager_secret db_admin {
  name        = "/${var.environment}/postgres/admin"
  description = "${var.environment} postgresql pgadmin password"
  kms_key_id  = var.kms_key_id
}

resource aws_secretsmanager_secret_version db_admin {
  secret_id     = aws_secretsmanager_secret.db_admin.id
  secret_string = var.admin_password
}
