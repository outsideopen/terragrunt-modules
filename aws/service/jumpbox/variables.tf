## VPC settings
variable vpc_id {}
variable allowed_cidr_blocks {
  type    = list(string)
  default = [
    "0.0.0.0/0",
  ]
}
variable ingress_security_groups {
  type        = list(string)
  description = "AWS security group IDs allowed ingress to instance"
  default     = []
}

variable security_groups {
  type        = list(string)
  description = "AWS security group IDs associated with instance"
  default     = []
}

### OTHER
variable instance_ami {}
variable key_name {}
variable subnets {
  type = list(string)
}
variable instance_type {
  type    = string
  default = "t3a.micro"
}
variable enable_eip {
  type    = bool
  default = true
}
variable root_block_device_volume_size {
  default = 20
}

