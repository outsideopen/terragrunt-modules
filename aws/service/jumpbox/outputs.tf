output id {
  value = module.instance.instance_id
}

output hostname {
  value = module.instance.hostname
}

output public_ip {
  value = var.enable_eip ? aws_eip.instance[0].public_ip : module.instance.public_ip
}

output public_dns {
  value = var.enable_eip ? aws_eip.instance[0].public_dns : module.instance.public_dns
}

output private_ip {
  value = module.instance.private_ip
}

output security_group_id {
  value = module.instance.security_group_id
}

output role {
  value = module.instance.role
}