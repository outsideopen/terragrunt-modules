module "instance" {
  # forked locally due to missing egress rule
  # https://github.com/cloudposse/terraform-aws-ec2-bastion-server/pull/29
  source                        = "./ec2-bastion-server"
  namespace                     = var.namespace
  stage                         = var.environment
  name                          = module.this.name
  # VPC
  vpc_id                        = var.vpc_id
  subnets                       = var.subnets
  # security
  ingress_security_groups       = var.ingress_security_groups
  security_groups               = var.security_groups
  egress_allowed                = true
  # root disk
  root_block_device_encrypted   = true
  root_block_device_volume_size = var.root_block_device_volume_size
  ## instance information
  ami                           = var.instance_ami
  instance_type                 = var.instance_type
  key_name                      = var.key_name
  ssh_user                      = "ubuntu"
}

resource "aws_eip" "instance" {
  count    = var.enable_eip ? 1 : 0
  vpc      = true
  instance = module.instance.instance_id
  tags     = {
    Name = "${var.namespace}-${var.environment}-${var.name}"
  }
}