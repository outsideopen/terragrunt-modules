locals {
  execution_role_policy_arns = distinct(concat(var.execution_policy_arns, [
    "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
  ]))
}

module execution_role {
  source  = "cloudposse/iam-role/aws"
  version = "0.6.1"
  context = module.this.context
  name    = "execution-role"

  role_description   = "Waypoint Execution Role for ${module.this.id}"
  policy_description = "Waypoint Execution Role"

  assume_role_action = "sts:AssumeRole"
  principals         = {
    Service = [
      "ec2.amazonaws.com",
      "ecs-tasks.amazonaws.com"
    ]
  }

  policy_document_count = length(var.execution_policies)
  policy_documents      = var.execution_policies
}

resource aws_iam_role_policy_attachment execution_role_policies {
  for_each   = toset(local.execution_role_policy_arns)
  role       = module.execution_role.name
  policy_arn = each.value
}