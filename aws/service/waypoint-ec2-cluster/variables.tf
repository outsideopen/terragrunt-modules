variable region {
  type        = string
  description = "Region"
}

variable vpc_id {
  type        = string
  description = "VPC ID"
}

variable subnet_ids {
  type        = list(string)
  description = "List of subnets"
}

variable allowed_cidr_blocks {
  type        = list(string)
  description = "List of allowed cidr blocks to access the instance"
}

variable instance_type {
  type        = string
  description = "The instance size"
  default     = "t3a.small"
}

variable key_name {
  type        = string
  description = "The keypair to use for ssh access"
}

variable root_storage_size {
  description = "The size of the root storage for the instances"
  default     = 40
}

variable min {
  default = 1
}

variable max {
  default = 3
}

variable desired_count {
  default = 1
}

variable execution_policies {
  default = []
}

variable execution_policy_arns {
  type        = list(string)
  description = "List of policy ARN's to attach to the execution role"
  default     = []
}

