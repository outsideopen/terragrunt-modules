output name {
  value = aws_ecs_cluster.cluster.name
}

output id {
  value = aws_ecs_cluster.cluster.id
}

output security_group_id {
  value = module.allow_ingress_any.this_security_group_id
}

output execution_role_name {
  value = module.execution_role.name
}

output execution_role_arn {
  value = module.execution_role.arn
}