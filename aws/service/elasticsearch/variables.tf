### common variables
variable namespace {}
variable environment {}
variable name {
  default = "elasticsearch"
}

### VPC variables
variable vpc_id {}
variable security_groups {
  type    = list(string)
  default = []
}
variable allowed_cidr_blocks {
  type    = list(string)
  default = []
}
variable subnets {
  type = list
}

## instance settings
variable storage_size {
  default = 10
}

variable instance_count {
  default = 1
}

variable instance_type {
  default = "t3.small.elasticsearch"
}

## elasticsearch variables
variable warm_enabled {
  default = false
}
variable zone_awareness_enabled {
  default = false
}