output hostname {
  value = module.elasticsearch.domain_hostname
}

output endpoint {
  value = module.elasticsearch.domain_endpoint
}

output kibana_endpoint {
  value = module.elasticsearch.kibana_endpoint
}

output kibana_hostname {
  value = module.elasticsearch.kibana_hostname
}

output param_elasticsearch_endpoint_arn {
  value = aws_ssm_parameter.elasticsearch_endpoint.arn
}

output param_elasticsearch_url_arn {
  value = aws_ssm_parameter.elasticsearch_url.arn
}

output param_kibana_endpoint_arn {
  value = aws_ssm_parameter.elasticsearch_kibana.arn
}