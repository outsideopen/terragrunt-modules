module "label" {
  source      = "cloudposse/label/null"
  version     = "0.19.2"
  namespace   = var.namespace
  environment = var.environment
  stage       = ""
  name        = var.name
}

module "elasticsearch" {
  source  = "cloudposse/elasticsearch/aws"
  version = "0.23.0"
  context = module.label.context

  ## VPC
  vpc_id              = var.vpc_id
  allowed_cidr_blocks = var.allowed_cidr_blocks
  security_groups     = var.security_groups
  subnet_ids          = [var.subnets[0]]

  ## instance
  instance_count  = var.instance_count
  instance_type   = var.instance_type
  ebs_volume_size = var.storage_size

  ## elasticsearch
  encrypt_at_rest_enabled               = true
  node_to_node_encryption_enabled       = true
  domain_endpoint_options_enforce_https = true
  kibana_subdomain_name                 = "${module.label.id}-kibana"
  warm_enabled                          = var.warm_enabled
  warm_count                            = 2
  zone_awareness_enabled                = var.zone_awareness_enabled
  advanced_options                      = {
    "rest.action.multi.allow_explicit_index" = "true"
  }
}

resource "aws_elasticsearch_domain_policy" "es_policy" {
  domain_name     = module.elasticsearch.domain_name
  access_policies = <<POLICIES
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "es:*",
      "Effect": "Allow",
      "Principal": {
        "AWS" : "*"
      },
      "Resource": "${module.elasticsearch.domain_arn}/*"
    }
  ]
}
POLICIES
}

resource aws_ssm_parameter elasticsearch_endpoint {
  name        = "/${var.environment}/elasticsearch/endpoint"
  description = "${var.environment} elasticsearch endpoint"
  type        = "String"
  value       = module.elasticsearch.domain_endpoint
}

resource aws_ssm_parameter elasticsearch_kibana {
  name        = "/${var.environment}/elasticsearch/kibana"
  description = "${var.environment} elasticsearch kiban endpoint"
  type        = "String"
  value       = module.elasticsearch.kibana_endpoint
}

resource aws_ssm_parameter elasticsearch_url {
  name        = "/${var.environment}/elasticsearch/url"
  description = "${var.environment} elasticsearch url"
  type        = "String"
  value       = "https://${module.elasticsearch.domain_endpoint}:443"
}