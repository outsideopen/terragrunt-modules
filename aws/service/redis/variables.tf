### common variables
variable namespace {}
variable environment {}
variable name {
  default = "redis"
}

variable kms_key_id {}

### VPC variables
variable vpc_id {}
variable allowed_security_groups {
  default = []
}
variable allowed_cidr_blocks {
  type    = list(string)
  default = []
}
variable existing_security_groups {
  default = []
}

variable subnets {
  type = list
}
variable az_names {
  type = list
}

## instance settings
variable cluster_mode_enabled {
  default = false
}
variable cluster_size {
  default = 1
}
variable instance_type {
  default = "cache.t2.micro"
}

## redis
variable auth_token {}
variable engine_version {
  default = "5.0.6"
}
variable engine_family {
  default = "redis5.0"
}
