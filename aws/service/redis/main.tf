module "redis" {
  source    = "cloudposse/elasticache-redis/aws"
  version   = "0.25.0"
  namespace = var.namespace
  stage     = var.environment
  name      = "api-redis"

  ## VPC
  vpc_id                  = var.vpc_id
  availability_zones      = var.az_names
  subnets                 = var.subnets
  allowed_cidr_blocks     = var.allowed_cidr_blocks
  allowed_security_groups = var.allowed_security_groups
  //  existing_security_groups     = var.existing_security_groups
  //  use_existing_security_groups = true

  ## instance
  cluster_mode_enabled = var.cluster_mode_enabled
  cluster_size         = var.cluster_size
  instance_type        = var.instance_type
  engine_version       = var.engine_version
  family               = var.engine_family

  auth_token                 = var.auth_token
  at_rest_encryption_enabled = true
  transit_encryption_enabled = true
}

resource aws_ssm_parameter redis_id {
  name        = "/${var.environment}/redis/id"
  description = "${var.environment} redis id"
  type        = "String"
  value       = module.redis.id
}

resource aws_ssm_parameter redis_endpoint {
  name        = "/${var.environment}/redis/endpoint"
  description = "${var.environment} redis endpoint"
  type        = "String"
  value       = module.redis.endpoint
}

resource aws_secretsmanager_secret redis_token {
  name        = "/${var.environment}/redis/token"
  description = "${var.environment} redis auth token"
  kms_key_id  = var.kms_key_id
}

resource aws_secretsmanager_secret_version redis_token {
  secret_id     = aws_secretsmanager_secret.redis_token.id
  secret_string = var.auth_token
}