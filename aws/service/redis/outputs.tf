output address {
  value = module.redis.host
}

output endpoint {
  value = module.redis.endpoint
}

output id {
  value = module.redis.id
}

output param_endpoint_arn {
  value = aws_ssm_parameter.redis_endpoint.arn
}

output secret_token_arn {
  value = aws_secretsmanager_secret_version.redis_token.arn
}

output secret_token_version_id {
  value = aws_secretsmanager_secret_version.redis_token.version_id
}