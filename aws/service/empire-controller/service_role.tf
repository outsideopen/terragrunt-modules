data aws_iam_policy_document service_role_policy {
  statement {
    sid       = "empireServiceRole"
    actions   = [
      "ec2:Describe*",
      "elasticloadbalancing:*",
      "ecs:*",
      "iam:ListInstanceProfiles",
      "iam:ListRoles",
      "iam:PassRole",
      "route53:*",
      "lambda:InvokeFunction"
    ]
    resources = [
      "*"
    ]
  }

  statement {
    sid       = "empireEcsTasks"
    actions   = [
      "ecs:RunTask"
    ]
    resources = [
      "*"
    ]
    condition {
      test     = "ArnEquals"
      variable = "ecs:cluster"
      values   = [
        var.runner_cluster_arn
      ]
    }
  }
}

module service_role {
  source  = "cloudposse/iam-role/aws"
  version = "0.6.1"
  context = module.this.context
  name    = "empire-service"

  policy_description = "Allow Empire to access AWS resources"
  role_description   = "Allows Empire to access AWS resources"

  assume_role_action = "sts:AssumeRole"
  principals         = {
    Service = [
      "ecs.amazonaws.com",
      "events.amazonaws.com",
      "lambda.amazonaws.com"
    ]
  }

  policy_documents = [
    data.aws_iam_policy_document.service_role_policy.json
  ]
}