data aws_iam_policy_document empire_role_policy {
  statement {
    sid       = "EmpireLogging"
    actions   = [
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = [
      "arn:aws:logs:*:*:log-group:${aws_cloudwatch_log_group.app.name}:log-stream:*",
      "arn:aws:logs:*:*:log-group:${aws_cloudwatch_log_group.controller.name}:log-stream:*",
      "arn:aws:logs:*:*:log-group:${aws_cloudwatch_log_group.postgres.name}:log-stream:*",
    ]
  }

  statement {
    sid       = "EmpireAllowCustomResourceQeueue"
    actions   = [
      "sqs:ReceiveMessage",
      "sqs:DeleteMessage",
      "sqs:ChangeMessageVisibility"
    ]
    resources = [
      aws_sqs_queue.custom_resources.arn
    ]
  }
  statement {
    sid       = "EmpireAllowCustomResourceTopic"
    actions   = [
      "sns:Publish"
    ]
    resources = [
      aws_sns_topic.custom_resources.arn,
      aws_sns_topic.events.arn
    ]
  }

  statement {
    sid       = "EmpireAllowS3TemplateBucketAccess"
    actions   = [
      "s3:PutObject",
      "s3:PutObjectAcl",
      "s3:PutObjectVersionAcl",
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:GetObjectAcl",
      "s3:GetObjectVersionAcl"
    ]
    resources = [
      "${module.template_bucket.bucket_arn}/*"
    ]
  }
  statement {
    sid       = "EmpireAllowAccessToServices"
    actions   = [
      # iam
      "iam:PassRole",
      # lamda
      "lambda:CreateFunction",
      "lambda:DeleteFunction",
      "lambda:UpdateFunctionCode",
      "lambda:GetFunctionConfiguration",
      "lambda:AddPermission",
      "lambda:RemovePermission",
      # events
      "events:PutRule",
      "events:DeleteRule",
      "events:DescribeRule",
      "events:EnableRule",
      "events:DisableRule",
      "events:PutTargets",
      "events:RemoveTargets",
      # allow cloudformation
      "cloudformation:CreateStack",
      "cloudformation:UpdateStack",
      "cloudformation:DeleteStack",
      "cloudformation:ListStackResources",
      "cloudformation:DescribeStackResource",
      "cloudformation:DescribeStacks",
      "cloudformation:ValidateTemplate",
      # allow ecs
      "ecs:CreateService",
      "ecs:DeleteService",
      "ecs:DeregisterTaskDefinition",
      "ecs:Describe*",
      "ecs:List*",
      "ecs:RegisterTaskDefinition",
      "ecs:RunTask",
      "ecs:StartTask",
      "ecs:StopTask",
      "ecs:SubmitTaskStateChange",
      "ecs:UpdateService",
      # allow elb
      "elasticloadbalancing:*",
      # allow ec2 describing
      "ec2:DescribeSubnets",
      "ec2:DescribeSecurityGroups",
      "ec2:DescribeInstances"
    ]
    resources = ["*"]
  }
  statement {
    sid       = "EmpireAllowInternalDomainAccess"
    actions   = [
      "route53:ListHostedZonesByName",
      "route53:ChangeResourceRecordSets",
      "route53:ListResourceRecordSets",
      "route53:ListHostedZones",
      "route53:GetHostedZone"
    ]
    resources = [
      "arn:aws:route53:::hostedzone/${var.runner_dns_zone_id}"
    ]
  }
  statement {
    sid       = "EmpireAllowRoute53ChangeAccess"
    actions   = [
      "route53:GetChange*"
    ]
    resources = [
      "arn:aws:route53:::change/*"
    ]
  }
}

module empire_role {
  source  = "cloudposse/iam-role/aws"
  version = "0.6.1"
  context = module.this.context
  name    = "empire"

  policy_description = "Allow Empire to access ECS tasks"
  role_description   = "Allows Empire to access ECS tasks"

  assume_role_action = "sts:AssumeRole"
  principals         = {
    Service = [
      "ecs-tasks.amazonaws.com"
    ]
  }

  policy_documents = [
    data.aws_iam_policy_document.empire_role_policy.json
  ]
}