variable aws_id {}
variable region {}
variable host_port {}

### Log settings
variable retain_app_logs_in_days {
  default = 7
}

variable retain_controller_logs_in_days {
  default = 7
}

variable retain_postgres_logs_in_days {
  default = 7
}

### Cluster settings
variable controller_vpc_id {}
variable controller_role_name {}
variable controller_profile_name {}
variable controller_public_subnets {
  type = list(string)
}
variable controller_security_group_id {}
variable controller_cluster_name {}
variable controller_cluster_id {
  description = "Where the Empire Controller should be run"
}

### ssl self signed settings
variable ssl_host {}
variable ssl_common_name {}
variable ssl_organization {}

### Container settings
variable desired_count {
  default = 1
}
variable image {
  default = "outsideopen/empire"
}
variable image_version {
  default = "master"
}
variable log_retention_in_days {
  default = 90
}

### empire settings
# primary
variable db_user {
  default = "empire"
}
variable db_pass {
  default = "epoo9mohNg"
}
variable db_host {
  default = "db"
}
variable db_name {
  default = "empire"
}

## runners
variable runner_vpc_id {}
variable runner_cluster_arn {}
variable runner_cluster_name {}
variable runner_dns_zone_id {}
variable runner_security_group_private {
  default = null
}
variable runner_security_group_public {
  default = null
}
variable runner_subnets_private {
  type = list(string)
}
variable runner_subnets_public {
  type = list(string)
}

# optional - docker auth
variable docker_registry {
  default = ""
}

variable docker_user {
  default = ""
}
variable docker_pass {
  default = ""
}
variable docker_email {
  default = ""
}
# optional - github auth
variable github_client_id {
  default = ""
}
variable github_client_secret {
  default = ""
}
variable github_organization {
  default = ""
}
variable github_team_id {
  default = ""
}
