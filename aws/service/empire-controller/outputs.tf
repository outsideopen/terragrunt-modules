output name {
  value = module.this.id
}

output endpoint {
  value = aws_elb.controller.dns_name
}
