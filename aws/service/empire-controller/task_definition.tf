module container {
  source  = "cloudposse/ecs-container-definition/aws"
  version = "0.42.0"

  # definition
  essential        = true
  container_name   = module.this.id
  container_image  = "${var.image}:${var.image_version}"
  container_memory = 256
  container_cpu    = 256
  map_environment  = local.empire_environment_default
  port_mappings    = [
    {
      hostPort      = var.host_port
      containerPort = var.host_port
      protocol      = "tcp"
    }
  ]
  entrypoint       = []
  command          = [
    "server",
    "-automigrate=true"
  ]
  mount_points     = [
    {
      sourceVolume  = "dockerSocket"
      containerPath = "/var/run/docker.sock"
      readOnly      = false
    },
    {
      sourceVolume  = "dockerCfg"
      containerPath = "/root/.dockercfg"
      readOnly      = false
    }
  ]

  log_configuration = {
    logDriver     = "awslogs"
    options       = {
      "awslogs-region"        = var.region
      "awslogs-group"         = join("", aws_cloudwatch_log_group.controller.*.name)
      "awslogs-stream-prefix" = coalesce(var.name, module.this.id)
    }
    secretOptions = null
  }
}

resource aws_ecs_task_definition controller {
  family                = module.this.id
  task_role_arn         = module.empire_role.arn
  container_definitions = module.container.json_map_encoded_list

  volume {
    name      = "dockerSocket"
    host_path = "/var/run/docker.sock"
  }

  volume {
    name      = "dockerCfg"
    host_path = "/home/ec2-user/.dockercfg"
  }
}