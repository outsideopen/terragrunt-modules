module allow_web_access {
  source      = "terraform-aws-modules/security-group/aws"
  version     = "3.16.0"
  name        = "${module.this.id}--web-access"
  tags        = merge(module.this.tags, {
    Name = "${module.this.id}-web-access"
  })
  description = "HTTP/HTTPS access for ${var.environment}"
  vpc_id      = var.runner_vpc_id

  ingress_rules       = [
    "http-80-tcp",
    "https-443-tcp"
  ]
  ingress_cidr_blocks = [
    "0.0.0.0/0"
  ]
  egress_rules        = [
    "all-all"
  ]
  egress_cidr_blocks  = [
    "0.0.0.0/0"
  ]

  # disable ipv6
  ingress_ipv6_cidr_blocks = []
  egress_ipv6_cidr_blocks  = []
}
