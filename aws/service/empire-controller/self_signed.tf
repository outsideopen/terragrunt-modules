// this is to get it up and running
resource tls_private_key self_signed_ssl {
  algorithm = "RSA"
  rsa_bits  = 2048
}

resource tls_self_signed_cert self_signed_ssl {
  key_algorithm         = "RSA"
  private_key_pem       = tls_private_key.self_signed_ssl.private_key_pem
  subject {
    common_name  = var.ssl_common_name
    organization = var.ssl_organization
  }
  // 6 months
  dns_names             = [
    var.ssl_host
  ]
  validity_period_hours = 4320
  allowed_uses          = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]
}

resource aws_acm_certificate ssl_cert {
  private_key      = tls_private_key.self_signed_ssl.private_key_pem
  certificate_body = tls_self_signed_cert.self_signed_ssl.cert_pem
}