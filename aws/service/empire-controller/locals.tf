locals {
  empire_environment_default = {
    AWS_REGION                      = var.region
    EMPIRE_PORT                     = var.host_port
    EMPIRE_SERVER_AUTH              = "local"
    EMPIRE_SCHEDULER                = "cloudformation"
    EMPIRE_ENVIRONMENT              = coalesce(var.environment, "demo")
    EMPIRE_DATABASE_URL             = "postgres://${var.db_user}:${var.db_pass}@${var.db_host}/${var.db_name}"
    EMPIRE_S3_TEMPLATE_BUCKET       = module.template_bucket.bucket_id
    EMPIRE_ECS_CLUSTER              = var.runner_cluster_name
    EMPIRE_ECS_LOG_DRIVER           = "awslogs"
    EMPIRE_ECS_LOG_OPT              = "awslogs-region=${var.region},awslogs-group=${aws_cloudwatch_log_group.app.name}"
    EMPIRE_ECS_SERVICE_ROLE         = module.service_role.name
    EMPIRE_ELB_VPC_ID               = var.runner_vpc_id
    // default SG is allow 80/443
    EMPIRE_ELB_SG_PRIVATE           = coalesce(var.runner_security_group_private, module.allow_web_access.this_security_group_id)
    EMPIRE_ELB_SG_PUBLIC            = coalesce(var.runner_security_group_public, module.allow_web_access.this_security_group_id)
    EMPIRE_ROUTE53_INTERNAL_ZONE_ID = var.runner_dns_zone_id
    EMPIRE_EC2_SUBNETS_PRIVATE      = join(",", var.runner_subnets_private)
    EMPIRE_EC2_SUBNETS_PUBLIC       = join(",", var.runner_subnets_public)
    EMPIRE_EVENTS_BACKEND           = "sns"
    EMPIRE_SNS_TOPIC                = aws_sns_topic.events.arn
    EMPIRE_RUN_LOGS_BACKEND         = "cloudwatch"
    EMPIRE_CLOUDWATCH_LOG_GROUP     = aws_cloudwatch_log_group.app.name
    EMPIRE_CUSTOM_RESOURCES_TOPIC   = aws_sns_topic.custom_resources.arn
    EMPIRE_CUSTOM_RESOURCES_QUEUE   = aws_sqs_queue.custom_resources.name
    EMPIRE_GITHUB_CLIENT_ID         = var.github_client_id
    EMPIRE_GITHUB_CLIENT_SECRET     = var.github_client_secret
    EMPIRE_GITHUB_ORGANIZATION      = var.github_organization
    EMPIRE_GITHUB_TEAM_ID           = var.github_team_id
    EMPIRE_X_SHOW_ATTACHED          = "true"
    EMPIRE_SERVER_REAL_IP           = "X-Forwarded-For"
  }
  empire_environment         = flatten([
  for env_name, env_value in local.empire_environment_default : {
    name  = env_name
    value = env_value
  }
  ])
  empire_test_vars           = {
    AWS_REGION          = "asdf"
    EMPIRE_SCHEDULER    = "cloudformation"
    EMPIRE_ENVIRONMENT  = "demo"
    EMPIRE_DATABASE_URL = "postgres:/"
  }
  empire_env                 = flatten([
  for env_name, env_value in local.empire_test_vars : {
    name  = env_name
    value = env_value
  }
  ])
}