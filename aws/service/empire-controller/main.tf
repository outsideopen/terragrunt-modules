resource aws_cloudwatch_log_group controller {
  name              = "${module.this.id}-controller"
  tags              = merge(module.this.tags, {
    Name = "${module.this.id}-controller"
  })
  retention_in_days = var.retain_controller_logs_in_days
}

resource aws_cloudwatch_log_group app {
  name              = "${module.this.id}-app"
  tags              = merge(module.this.tags, {
    Name = "${module.this.id}-app"
  })
  retention_in_days = var.retain_app_logs_in_days
}

resource aws_cloudwatch_log_group postgres {
  name              = "${module.this.id}-postgres"
  tags              = merge(module.this.tags, {
    Name = "${module.this.id}-postgres"
  })
  retention_in_days = var.retain_postgres_logs_in_days
}

resource aws_sns_topic custom_resources {
  name         = "${module.this.id}-custom-resources"
  display_name = "${module.this.environment} Empire Custom Resources"
}

resource aws_sns_topic_subscription custom_resources {
  topic_arn = aws_sns_topic.custom_resources.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.custom_resources.arn
}

resource aws_sqs_queue custom_resources {
  name = "${module.this.id}-custom-resources"
  tags = merge(module.this.tags, {
    Name = "${module.this.id}-custom-resources"
  })
}

resource aws_sqs_queue_policy sqs_policy {
  queue_url = aws_sqs_queue.custom_resources.id
  policy    = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "CustomResourcesQueuePolicy",
    "Statement": [
        {
            "Sid": "AllowCustomResourcesTopicToSendMessages",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "sqs:SendMessage",
            "Resource": "${aws_sqs_queue.custom_resources.arn}",
            "Condition": {
                "ArnEquals": {
                  "aws:SourceArn": "${aws_sns_topic.custom_resources.arn}"
                }
            }
        }
    ]
}
POLICY
}

resource aws_sns_topic events {
  name         = "${module.this.id}-events"
  display_name = "${module.this.environment} Empire events"
}

module template_bucket {
  source  = "cloudposse/s3-bucket/aws"
  version = "0.22.0"
  context = module.this.context
  name    = "empire-templates"
}


resource aws_elb controller {
  name                      = "${module.this.id}-controller"
  subnets                   = var.controller_public_subnets
  security_groups           = [
    var.controller_security_group_id
  ]
  cross_zone_load_balancing = true

  listener {
    instance_port      = var.host_port
    instance_protocol  = "http"
    lb_port            = 443
    lb_protocol        = "https"
    ssl_certificate_id = aws_acm_certificate.ssl_cert.arn
  }

  health_check {
    healthy_threshold   = 10
    unhealthy_threshold = 2
    timeout             = 5
    target              = "HTTP:${var.host_port}/health"
    interval            = 30
  }
  idle_timeout = 3600
}

resource aws_ecs_service empire {
  name                 = "${module.this.id}-empire-controller"
  cluster              = var.controller_cluster_id
  task_definition      = aws_ecs_task_definition.controller.arn
  desired_count        = 1
  iam_role             = module.service_role.arn
  force_new_deployment = true
  depends_on           = [
    module.service_role
  ]

  load_balancer {
    elb_name       = aws_elb.controller.name
    container_name = module.this.id
    container_port = var.host_port
  }
}

// attach an extra policy for logs to the controller_instance_profile
data aws_iam_policy_document log_group_access {
  statement {
    sid       = "InstanceControllerLogging"
    actions   = [
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = [
      "arn:aws:logs:*:*:log-group:${aws_cloudwatch_log_group.app.name}:log-stream:*",
      "arn:aws:logs:*:*:log-group:${aws_cloudwatch_log_group.controller.name}:log-stream:*",
      "arn:aws:logs:*:*:log-group:${aws_cloudwatch_log_group.postgres.name}:log-stream:*",
    ]
  }
}

resource aws_iam_policy log_group_access {
  name   = "${module.this.id}-log-write"
  policy = data.aws_iam_policy_document.log_group_access.json
}

resource aws_iam_role_policy_attachment log_group_access {
  role       = var.controller_role_name
  policy_arn = aws_iam_policy.log_group_access.arn
}
