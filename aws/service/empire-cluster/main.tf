data aws_ssm_parameter ecs_optimized_ami {
  name = "/aws/service/ecs/optimized-ami/amazon-linux-2/recommended"
}

data template_file empire_user_data {
  template = file("user_data.sh")
  vars     = {
    cluster_id      = aws_ecs_cluster.empire.name
    docker_registry = var.docker_registry
    docker_user     = var.docker_user
    docker_pass     = var.docker_pass
    docker_email    = var.docker_email
  }
}

resource aws_ecs_cluster empire {
  name = module.this.id
  tags = module.this.tags
}

module allow_ingress_any {
  source              = "terraform-aws-modules/security-group/aws"
  version             = "3.16.0"
  name                = "${module.this.id}-any-in"
  description         = "Container Instance Allowed Ports"
  vpc_id              = var.vpc_id
  ingress_rules       = [
    "all-all"
  ]
  ingress_cidr_blocks = var.allowed_cidr_blocks

  egress_rules = [
    "all-all"
  ]
  egress_cidr_blocks = [
    "0.0.0.0/0"
  ]
}

resource aws_launch_configuration empire {
  name_prefix                 = "${module.this.id}-"
  image_id                    = jsondecode(data.aws_ssm_parameter.ecs_optimized_ami.value).image_id
  instance_type               = var.instance_type
  iam_instance_profile        = aws_iam_instance_profile.instance.name
  key_name                    = var.key_name
  security_groups             = [
    module.allow_ingress_any.this_security_group_id
  ]
  associate_public_ip_address = false
  user_data                   = data.template_file.empire_user_data.rendered
  ebs_optimized               = true

  root_block_device {
    encrypted   = true
    volume_type = "gp2"
    volume_size = var.root_storage_size
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource aws_autoscaling_group empire {
  name                 = module.this.id
  vpc_zone_identifier  = var.subnet_ids
  launch_configuration = aws_launch_configuration.empire.name
  min_size             = var.min
  max_size             = var.max
  desired_capacity     = var.desired_capacity

  tags = flatten([
  for key in keys(module.this.tags) :
  {
    key                 = key
    value               = module.this.tags[key]
    propagate_at_launch = true
  }
  ])

  lifecycle {
    create_before_destroy = true
  }
}
