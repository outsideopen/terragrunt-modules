output name {
	value = aws_iam_instance_profile.instance.name
}

output arn {
  value = aws_iam_instance_profile.instance.arn
}

output path {
  value = aws_iam_instance_profile.instance.path
}