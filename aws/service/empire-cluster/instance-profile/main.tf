data aws_iam_policy_document assume_role_policy {
  statement {
    sid     = "AllowAssumeRole"
    effect  = "Allow"
    actions = [
      "sts:AssumeRole"
    ]
    principals {
      type        = "Service"
      identifiers = [
        "ec2.amazonaws.com"
      ]
    }
  }
}

resource aws_iam_role instance {
  name               = module.this.id
  path               = "/"
  description        = "Role for ${module.this.id} instances"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
}

resource aws_iam_instance_profile instance {
  name = module.this.id
  role = aws_iam_role.instance.name
}

resource aws_iam_role_policy_attachment instance {
  count      = length(var.policy_arns)
  //  for_each   = var.policy_arns
  role       = aws_iam_role.instance.name
  //  policy_arn = each.value
  policy_arn = element(var.policy_arns, count.index)
}