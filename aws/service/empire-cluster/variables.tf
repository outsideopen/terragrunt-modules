# VPC settings
variable vpc_id {}
variable availability_zones {
  type = list(string)
}
variable subnet_ids {
  type = list(string)
}
variable allowed_cidr_blocks {
  type = list(string)
}
variable profile_policy_arns {
  type    = set(string)
  default = []
}

# instance settings
variable key_name {}

variable instance_type {
  default = "t3a.small"
}

variable root_storage_size {
  default = 40
}

# docker authenticaiton settings
variable docker_registry {
  default = "https://index.docker.io/v1/"
}
variable docker_user {
  default = ""
}
variable docker_pass {
  default = ""
}
variable docker_email {
  default = ""
}

# cluster settings
variable desired_capacity {
  default = 2
}
variable min {
  default = 1
}
variable max {
  default = 3
}
variable scale_down_by {
  default = 1
}
variable scale_down_cooldown {
  default = 60
}
variable scale_up_by {
  default = 1
}
variable scale_up_cooldown {
  default = 60
}