data aws_iam_policy_document ecs_agent_policy {
  statement {
    sid       = "empireEcsAgentPolicy"
    actions   = [
      "ecs:DeregisterContainerInstance",
      "ecs:DiscoverPollEndpoint",
      "ecs:Poll",
      "ecs:RegisterContainerInstance",
      "ecs:StartTelemetrySession",
      "ecs:Submit*",
      "ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage"
    ]
    resources = [
      "*"
    ]
  }
}

module instance_role {
  source  = "cloudposse/iam-role/aws"
  version = "0.6.1"
  context = module.this.context
  name    = "empire-controller"

  policy_description = "Allow Empire to access AWS resources"
  role_description   = "Allows Empire to access AWS resources"

  assume_role_action = "sts:AssumeRole"
  principals         = {
    Service = [
      "ec2.amazonaws.com"
    ]
  }

  policy_documents = [
    data.aws_iam_policy_document.ecs_agent_policy.json
  ]
}

resource aws_iam_role_policy_attachment instance_ecs_agent {
  role = module.instance_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource aws_iam_instance_profile instance {
  name = module.this.id
  role = module.instance_role.name
}