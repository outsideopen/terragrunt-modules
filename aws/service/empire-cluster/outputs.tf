output arn {
  value = aws_ecs_cluster.empire.arn
}

output id {
  value = aws_ecs_cluster.empire.id
}

output name {
  value = aws_ecs_cluster.empire.name
}

output security_group_id {
  value = module.allow_ingress_any.this_security_group_id
}

output asg_id {
  value = aws_autoscaling_group.empire.id
}

output asg_arn {
  value = aws_autoscaling_group.empire.arn
}

output profile_role_name {
  value = module.instance_role.name
}

output profile_role_arn {
  value = module.instance_role.arn
}

output profile {
  value = aws_iam_instance_profile.instance.name
}

output profile_arn {
  value = aws_iam_instance_profile.instance.arn
}