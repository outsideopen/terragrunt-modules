output id {
  value = module.instance.id
}

output private_dns {
  value = module.instance.private_dns
}

output private_ip {
  value = module.instance.private_ip
}

output public_dns {
  value = module.instance.public_dns
}

output public_ip {
  value = module.instance.public_ip
}

output security_group_ids {
  value = module.instance.security_group_ids
}

output role {
  value = module.instance.role
}