module instance {
  source                      = "cloudposse/ec2-instance/aws"
  version                     = "0.25.0"
  # info
  namespace                   = var.namespace
  stage                       = var.environment
  name                        = var.name
  # VPC
  vpc_id                      = var.vpc_id
  subnet                      = var.subnet
  assign_eip_address          = var.use_eip
  associate_public_ip_address = var.use_public_ip
  # security
  security_groups             = var.security_groups
  allowed_ports               = var.allowed_ports
  # root disk
  root_volume_size            = var.root_block_device_volume_size
  ## instance information
  ami                         = var.instance_ami
  ami_owner                   = var.ami_owner
  instance_type               = var.instance_type
  ssh_key_pair                = var.key_name
}