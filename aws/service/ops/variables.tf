variable namespace {
  type = string
}
variable environment {
  type = string
}
variable name {
  type    = string
}

## network settings
variable use_eip {
  default = false
}
variable use_public_ip {
  default = false
}
variable allowed_ports {
  type = list
  default = [22]
}

## VPC settings
variable vpc_id {}
variable allowed_cidr_blocks {
  type    = list(string)
  default = [
    "0.0.0.0/0",
  ]
}
variable ingress_security_groups {
  type        = list(string)
  description = "AWS security group IDs allowed ingress to instance"
  default     = []
}

variable security_groups {
  type        = list(string)
  description = "AWS security group IDs associated with instance"
  default     = []
}

### OTHER
variable instance_ami {}
variable ami_owner {}
variable key_name {}
variable subnet {}
variable instance_type {
  type    = string
  default = "t3a.nano"
}
variable enable_eip {
  type    = bool
  default = true
}
variable root_block_device_volume_size {
  default = 20
}

